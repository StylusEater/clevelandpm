package ClevelandPM;
use Mojo::Base 'Mojolicious';

sub startup {
    ## We need ourself to make it
    my $self = shift;

    ## Go go hypnotoad
    $self->app->config(hypnotoad => {listen => ['http://*:8080/']});

    ## Hoorary for Cleveland Perl Mongers
    ## --> Secret Key for session information
    ## 
    ## NOTE: Changed on the fly when we deploy.
    $self->secret('clevelandPerlMongers2013');

    ## Start setting up routes
    my $r = $self->routes;

    ## Route all requests to / to index
    $r->get('/')->to(template => 'index');

    ## Route all requests to /meetings/[date] to the proper meeting
    $r->get('/meetings/(:dateofmeeting)' => [dateofmeeting => qr/\d+/] => sub {
      my $self = shift;
      my $dateofmeeting = $self->param('dateofmeeting');
      $self->render($dateofmeeting);
    });

    ## Route all requests to /meetings to meetings
    $r->get('/meetings')->to(template => 'meetings');
    
    ## Route all requests to /presentations to presentations 
    $r->get('/presentations')->to(template => 'presentations');

    ## Route all requests to /links to links 
    $r->get('/links')->to(template => 'links');

}

1;
